package th.ac.tu.siit.calculator;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends Activity
	implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        ((Button)findViewById(R.id.num0)).setOnClickListener(this);
        ((Button)findViewById(R.id.num1)).setOnClickListener(this);
        ((Button)findViewById(R.id.num2)).setOnClickListener(this);
        ((Button)findViewById(R.id.num3)).setOnClickListener(this);
        ((Button)findViewById(R.id.num4)).setOnClickListener(this);
        ((Button)findViewById(R.id.num5)).setOnClickListener(this);
        ((Button)findViewById(R.id.num6)).setOnClickListener(this);
        ((Button)findViewById(R.id.num7)).setOnClickListener(this);
        ((Button)findViewById(R.id.num8)).setOnClickListener(this);
        ((Button)findViewById(R.id.num9)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.add)).setOnClickListener(this);
        ((Button)findViewById(R.id.sub)).setOnClickListener(this);
        ((Button)findViewById(R.id.mul)).setOnClickListener(this);
        ((Button)findViewById(R.id.div)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.ac)).setOnClickListener(this);
        ((Button)findViewById(R.id.bs)).setOnClickListener(this);
        
        ((Button)findViewById(R.id.dot)).setOnClickListener(this);
        ((Button)findViewById(R.id.equ)).setOnClickListener(this);
        
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    double input1 = 0.0;
    char sign = ' ';
    String output = "0";
    char operator = ' ';
    void computefunc(){
    	if (sign != ' ')
    	{
	    	TextView tvOutput = (TextView)findViewById(R.id.output);
			
	    	double input2 = Double.parseDouble(output);
	    	double res = 0;
			if (sign ==  ((Button)findViewById(R.id.add)).getText().toString().charAt(0)){
				 res = input1 + input2;
			}
	    	else if (sign == ((Button)findViewById(R.id.sub)).getText().toString().charAt(0)){
				 res = input1 - input2;
				}
	    	else if (sign == ((Button)findViewById(R.id.mul)).getText().toString().charAt(0)){
				 res = input1 * input2;
				}
	    	else if (sign == ((Button)findViewById(R.id.div)).getText().toString().charAt(0)){
				 res = input1 / input2;
				}
	    	
	    	output = String.valueOf(res);
			tvOutput.setText(output);
    	}
    }
	@Override
	public void onClick(View v) {
		int id =v.getId();
		
		TextView tvOutput = (TextView)findViewById(R.id.output);
		
		
		switch(id)
		{
		case R.id.num0:
		case R.id.num1:
		case R.id.num2:
		case R.id.num3:
		case R.id.num4:
		case R.id.num5:
		case R.id.num6:
		case R.id.num7:
		case R.id.num8:
		case R.id.num9:
			if(output.equals("0")){
				output = "";
			}
		
		output = output +((Button)v).getText().toString();
		tvOutput.setText(output);
		break;
		
		case R.id.ac:
			output = "0";
			input1 = 0.0;
			sign = ' ';
			tvOutput.setText(output);

			break;
		
		case R.id.equ:
			computefunc();
			tvOutput.setText(output);
			operator = 0;
			//op.setText("");
			sign= ' ';
			break;
			
		case R.id.dot:
			output += ".";
			tvOutput.setText(output);
			break;
			
		case R.id.bs:
			output = output.substring(0,output.length()-1);
			if(output.length()<=0)
			{output="0";}
			tvOutput.setText(output);
			break;
			
		case R.id.add:
		case R.id.sub:
		case R.id.mul:
		case R.id.div:
				//////////////
				
					computefunc();
			
				///////////////////
				
				input1 = Double.parseDouble(output);
				sign = ((Button)v).getText().toString().charAt(0);
				output ="0";
				
				TextView tv = (TextView)findViewById(R.id.operator);
				tv.setText(((Button)v).getText().toString());
		break;
		
			
	}
    
}
}